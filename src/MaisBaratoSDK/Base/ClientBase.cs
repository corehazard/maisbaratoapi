﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace MaisBaratoSDK.Base
{
    public abstract class ClientBase
    {
        public readonly HttpClient _client;
        public Uri BaseEndpoint { get; set; }

        public ClientBase(Uri baseEndpoint)
        {
            if (baseEndpoint == null)
            {
                throw new ArgumentNullException("BaseEndpoint cannot be null.");
            }

            this.BaseEndpoint = baseEndpoint;
            this._client = new HttpClient();
        }

        public Uri CreateRequestUri(string relativePath, string queryString = "")
        {
            var endpoint = new Uri(BaseEndpoint, relativePath);
            var uriBuilder = new UriBuilder(endpoint);
            uriBuilder.Query = queryString;
            return uriBuilder.Uri;
        }

        public HttpContent CreateHttpContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private void addHeaders()
        {
        }
    }
}
