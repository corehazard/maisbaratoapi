﻿using Hazard.BaseAPI.SDK.PnP;
using System.Collections.Generic;

namespace MaisBaratoAPI.Models
{
    public class NearSales
    {
        public Location Location
        {
            get; set;
        }

        public int Range
        {
            get; set;
        }

        public int Page
        {
            get; set;
        }

        public List<string> Categories
        {
            get; set;
        }
    }
}
