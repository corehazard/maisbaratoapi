﻿using Hazard.BaseAPI.SDK.Base;
using Hazard.BaseAPI.SDK.People;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MaisBaratoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : Controller
    {
        private readonly UserClient _userClient;

        public PeopleController(UserClient userClient)
        {
            _userClient = userClient;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] User user)
        {
            var createResult = await this._userClient.CreateUser(user);

            return Json(createResult);
        }


    }
}