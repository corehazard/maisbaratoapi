﻿using Hazard.BaseAPI.SDK.Base;
using Hazard.BaseAPI.SDK.PnP;
using MaisBaratoAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaisBaratoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly ProductClient _productAPIClient;
        private readonly PlaceClient _placeApiClient;

        #region Product Methods

        public ProductsController(ProductClient client, PlaceClient placeApiClient)
        {
            this._productAPIClient = client;
            this._placeApiClient = placeApiClient;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Product product)
        {
            var createResult = await this._productAPIClient.CreateProduct(product);
            return Json(new { Id = createResult.Id });
        }

        [HttpGet]
        [Route("{id:length(24)}")]
        public async Task<IActionResult> Get(string id)
        {
            return Json(await this._productAPIClient.GetProductById(id));
        }

        [HttpPost]
        [Route("_search")]
        public async Task<IActionResult> Get([FromBody] Product product)
        {
            return Json(await this._productAPIClient.Search(product));
        }

        [HttpPost]
        [Route("_place")]
        public async Task<IActionResult> Get([FromBody] Place place)
        {
            return Json(await this._productAPIClient.SearchByPlace(place));
        }

        [HttpPut]
        [Route("{id:length(24)}")]
        public NoContentResult Update(string id, [FromBody] Product product)
        {
            this._productAPIClient.Update(id, product);

            return NoContent();
        }

        [HttpDelete]
        [Route("{id:length(24)}")]
        public NoContentResult Delete(string id)
        {
            this._productAPIClient.Delete(id);

            return NoContent();
        }

        #endregion

        #region MethodsOutOfMainAPI

        [HttpPost]
        [Route("_nearSales")]
        public async Task<IActionResult> GetNearSales([FromBody] NearSales near)
        {
            var allSales = new List<Product>();
            var categories = new List<Category>();

            var nearestPlaces = await this._placeApiClient.SearchNear(near.Location, near.Range);

            if (near.Categories != null)
            {
                foreach (var category in near.Categories)
                {
                    var categoryFound = (await this._productAPIClient.GetCategories(category)).First();
                    categoryFound.Id = null;
                    categories.Add(categoryFound);
                }
            }

            foreach (var nearestPlace in nearestPlaces)
            {
                var sales = categories.Count > 0
                    ? (await this._productAPIClient.SearchByPlace(nearestPlace, categories)).Where(p => p.Status)
                    : (await this._productAPIClient.SearchByPlace(nearestPlace)).Where(p => p.Status);

                allSales.AddRange(sales);
            }

            var orderedSales = allSales.OrderByDescending(p => p.Timestamp).Skip((near.Page - 1) * 10).Take(10);

            return Json(new
            {
                sales = orderedSales,
                total = allSales.Count,
                limit = 10,
                page = near.Page,
                pages = Math.Ceiling((double)allSales.Count / 10)
            });
        }

        #endregion

        #region Category Methods

        [HttpGet]
        [Route("_category")]
        public async Task<IActionResult> GetCategories([FromQuery] string q = "")
        {
            return Json(await this._productAPIClient.GetCategories(q));
        }


        #endregion
    }
}