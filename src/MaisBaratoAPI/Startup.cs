﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using Hazard.BaseAPI.SDK.Base;

namespace MaisBaratoAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });


            var apiConfiguration = this.Configuration.GetSection("APIs");
            var pnpUrl = apiConfiguration["PnP"] ?? Environment.GetEnvironmentVariable("PNP_BASE_URI");
            var peopleUrl = apiConfiguration["People"] ?? Environment.GetEnvironmentVariable("PEOPLE_BASE_URI");

            if (string.IsNullOrEmpty(pnpUrl))
            {
                throw new InvalidDataException("You must configure a URI for PnP API.");
            }
            if (string.IsNullOrEmpty(peopleUrl))
            {
                throw new InvalidDataException("You must configure a URI for People API.");
            }

            var productClient = new ProductClient(new Uri(pnpUrl));
            var placeClient = new PlaceClient(new Uri(pnpUrl));
            var userClient = new UserClient(new Uri(pnpUrl));

            var builder = new Autofac.ContainerBuilder();

            builder.RegisterInstance(productClient).As<ProductClient>();
            builder.RegisterInstance(placeClient).As<PlaceClient>();
            builder.RegisterInstance(userClient).As<UserClient>();

            builder.Populate(services);

            var container = builder.Build();

            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
