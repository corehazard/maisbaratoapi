﻿using Hazard.BaseAPI.SDK.Base;
using Hazard.BaseAPI.SDK.PnP;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MaisBaratoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly ProductClient _productAPIClient;

        public ProductController(ProductClient client)
        {
            this._productAPIClient = client;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Product product)
        {
            return Json(new { Id = await this._productAPIClient.CreateProduct(product) });
        }

        [HttpGet]
        [Route("{id:length(24)}")]
        public async Task<IActionResult> Get(string id)
        {
            return Json(await this._productAPIClient.GetProductById(id));
        }

        [HttpPost]
        [Route("_search")]
        public async Task<IActionResult> Get([FromBody] Product product)
        {
            return Json(await this._productAPIClient.Search(product));
        }

        [HttpPost]
        [Route("_place")]
        public async Task<IActionResult> Get([FromBody] Place place)
        {
            return Json(await this._productAPIClient.SearchByPlace(place));
        }

        [HttpPut]
        [Route("{id:length(24)}")]
        public NoContentResult Update(string id, [FromBody] Product product)
        {
            this._productAPIClient.Update(id, product);

            return NoContent();
        }

        [HttpDelete]
        [Route("{id:length(24)}")]
        public NoContentResult Delete(string id)
        {
            this._productAPIClient.Delete(id);

            return NoContent();
        }
    }
}