﻿using System.Threading.Tasks;
using Hazard.BaseAPI.SDK.Base;
using Hazard.BaseAPI.SDK.PnP;
using Microsoft.AspNetCore.Mvc;

namespace MaisBaratoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaceController : Controller
    {
        private readonly PlaceClient _placeAPIClient;

        public PlaceController(PlaceClient client)
        {
            this._placeAPIClient = client;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Place place)
        {
            return Json(new { Id = await this._placeAPIClient.CreatePlace(place) });
        }

        [HttpGet]
        [Route("{id:length(24)}")]
        public async Task<IActionResult> Get(string id)
        {
            return Json(await this._placeAPIClient.GetPlaceById(id));
        }
        
        [HttpPost]
        [Route("_search")]
        public async Task<IActionResult> Get([FromBody] Place place)
        {
            return Json(await this._placeAPIClient.Search(place));
        }

        [HttpPost]
        [Route("_near")]
        public async Task<IActionResult> Get([FromBody] Location location, [FromQuery] int range)
        {
            return Json(await this._placeAPIClient.SearchNear(location, range));
        }

        [HttpPut]
        [Route("{id:length(24)}")]
        public NoContentResult Update(string id, [FromBody] Place place)
        {
            this._placeAPIClient.Update(id, place);

            return NoContent();
        }

        [HttpDelete]
        [Route("{id:length(24)}")]
        public NoContentResult Delete(string id)
        {
            this._placeAPIClient.Delete(id);

            return NoContent();
        }
    }
}